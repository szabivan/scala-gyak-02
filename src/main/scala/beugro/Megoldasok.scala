package beugro

/**
 * Próbálj idiomatikus Scala kódot írni, pl. ne használj mutable változókat.
 */
object Megoldasok extends App {

  /**
   *  Írj függvényt, ami visszaadja, hogy az input n számnak van-e az input k számnál kisebb valódi osztója!
   *  Pl. vanOszto( 4, 9 ) == true (mert a 3 valódi osztója 9-nek) és vanOszto( 3, 27 ) == false
   *  (mert 27-nek csak 3 és 9 a valódi osztói, amik nem kisebbek, mint 3)
   *  Feltehető, hogy n,k >= 1.
   */
  def vanOszto(k: Int, n: Int): Boolean = {
    def helper(m: Int): Boolean = 
      if (m < 2) false
      else if (n % m == 0) true
      else helper(m - 1)
    helper(Math.min(k, n) - 1)
  }

  /**
   *
   *  Akár felhasználva az előző függvényt, írj egy függvényt, ami visszaadja, hogy az input szám prím-e!
   *  Itt ügyelj a negatív számokra, nullára stb. is.
   */
  def isPrime(n: Int): Boolean = (n >= 2) && !vanOszto(n,n)

  /**
   *  Akár felhasználva az előző függvényeket, írj egy függvényt, ami visszaadja, hogy az input a és b
   *  számok közt hány a <= p <= b  prímszám van! (Ismét ügyelj arra, hogy valamelyik végpont lehet negatív is.)
   *  Ha rekurzív függvényt írsz, próbáld meg úgy megírni, hogy olyan paramétert ne adjon át, ami sosem változik!
   */
  def countPrimes(a: Int, b: Int): Int = {
    def helper(m: Int): Int =
      if (m>b) 0
      else (if (isPrime(m)) 1 else 0) + helper(m + 1)
    helper(a)
  }

  /**
   *  Írj függvényt, ami kap egy n >= 1 Int-et és visszaad egy String-et:
   *  - ha n 7-re végződik, de nem osztható 7-tel, akkor "Fizz"
   *  - ha n osztható 7-tel, de nem 7-re végződik, akkor "Buzz"
   *  - ha n osztható 7-tel és 7-re végződik, akkor "FizzBuzz"
   *  - ha n se 7-tel nem osztható és nem is 7-re végződik, akkor magát a számot Stringként (tízes számrendszerben)!
   *  hint: ugyanúgy lehet az Intet Stringre konvertálni ebben az utolsó esetben, mint ahogy az Intet Longra tettük.
   */
  def fizzBuzz(n: Int): String =
    if (n % 7 == 0) (if (n % 10 == 7) "FizzBuzz" else "Buzz")
    else if (n % 10 == 7) "Fizz" else n.toString

 
  /**
   * Írj függvényt, ami kap egy a és egy b Int-et és az összes a <= n <= b számra növekvő sorrendben
   * hívja a fizzBuzz függvényt, és kiírja a konzolra az eredményt!
   * (Ehhez a taskhoz nincs teszt.)
   */ 
  def printFizzBuzz(a: Int, b: Int): Unit = {
    def helper(m: Int): Unit =
      if (m>b) ()
      else {
        println(fizzBuzz(m))
        helper(m + 1)
      }
    helper(a)
  }

  /**
   * Írj függvényt, ami kap egy n pozitív egész számot, és visszaadja, hogy hányféleképp lehet az 1,2,3 számok
   * összegeként felírni úgy, hogy a sorrend is számít!
   * Tehát pl. ha az input n = 4, akkor a válasz 7, mert 4-et fel lehet írni mint
   * 1 + 1 + 1 + 1
   * 1 + 1 + 2
   * 1 + 2 + 1
   * 2 + 1 + 1
   * 2 + 2
   * 1 + 3
   * 3 + 1
   */ 
  def count123( n: Int ): Int = n match {
    case 1 => 1
    case 2 => 2
    case 3 => 4
    case _ => count123(n - 1) + count123(n - 2) + count123(n - 3)
  }

}
