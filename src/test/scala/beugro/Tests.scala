package beugro

import org.scalatest.flatspec.AnyFlatSpec
import beugro.Beugro._

import scala.util.Random

class VanOsztoTest extends AnyFlatSpec {
  "Van osztó" should "konstansokra" in {
    assert( vanOszto( 4, 9 ) == true, "vanOszto( 4, 9 ) == true kellene legyen!" )
    assert( vanOszto( 3, 27 ) == false, "vanOszto( 3, 27 ) == false kellene legyen!" )
    assert( vanOszto( 50, 41 ) == false, "vanOszto( 50, 41 ) == false kellene legyen!" )
  }
  it should "generált osztókra" in {
    for( _ <- 1 to 100 ) {
      val o = Random.nextInt(100) + 2
      val n = o * ( Random.nextInt(10) + 2 )
      val k = o + Random.nextInt(10) + 1
      assert( vanOszto( k, n ) == true, s"vanOszto( $k, $n ) == true kellene legyen!" )
    }
  }  
}

class PrimeTest extends AnyFlatSpec {
  "Prím" should "konstansokra" in {
    assert( isPrime( -7 ) == false, "isPrime( -7 ) == false kellene legyen!"  )
    assert( isPrime( 0 ) == false, "isPrime( 0 ) == false kellene legyen!"  )
    assert( isPrime( 1 ) == false, "isPrime( 1 ) == false kellene legyen!"  )
    assert( isPrime( 2 ) == true, "isPrime( 2 ) == true kellene legyen!"  )
    assert( isPrime( 7 ) == true, "isPrime( 7 ) == true kellene legyen!"  )
    assert( isPrime( 19 ) == true, "isPrime( 19 ) == true kellene legyen!"  )
  }
  it should "összeszámolva" in {
    assert( (1 to 100).count( isPrime ) == 25, "1 és 100 közt 25 prímszám van!" )
  }
}

class CountPrimesTest extends AnyFlatSpec {
  "CountPrimes" should "konstansokra" in {
    val theMap = Map( -100 -> 0, 0 -> 0, 100 -> 25, 1000 -> 168 )
    for( (lower, lowerCount) <- theMap ; (upper, upperCount) <- theMap ) {
      if( lower < upper ) assert( countPrimes(lower,upper) == theMap(upper) - theMap(lower), s"$lower és $upper közt ${theMap(upper)-theMap(lower)} prímszám van!" )
    }
  }
}

class FizzBuzzTest extends AnyFlatSpec {
  "FizzBuzz" should "konstansokra" in {
    val expectedMap = Map( 7 -> "FizzBuzz", 17 -> "Fizz", 27 -> "Fizz", 77 -> "FizzBuzz", 14 -> "Buzz", 21 -> "Buzz", 33 -> "33" )
    for( (input, expected) <- expectedMap ) {
      assert( fizzBuzz( input ) == expected, s"fizzBuzz( $input ) == $expected kéne legyen!" )
    }
  }
}

class Count123Test extends AnyFlatSpec {
  "Count123" should "konstansokra" in {
    val expectedMap = Map( 1 -> 1, 2 -> 2, 3 -> 4, 4 -> 7, 5 -> 13 )
    for( (input, expected) <- expectedMap ) {
      assert( count123( input ) == expected, s"count123( $input ) == $expected kéne legyen!" )
    }
  }
}
